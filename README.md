#ClassKick Interview Project 

Detects and finds if any student-sheet-object instances or sheet-object 
instances are orphaned.

Uses the python-firebase REST API to pull data
https://pypi.python.org/pypi/python-firebase/1.2

Example Output:
No orphaned sheet_object instances found!

Orphaned student_sheet_object instances:
-JKwNA-FCkXFz0xahbHe
-JKwNSfYtNpbv8OpQ4aJ
-JKwOd1ftpGHBPngTIFU
-JKwOdZxUmowCrIwvzFJ
-JKwOdKnKtZdVibzIKCK
