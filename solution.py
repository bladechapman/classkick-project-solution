from firebase import firebase
firebase = firebase.FirebaseApplication(
    'https://intense-fire-3287.firebaseio.com/', None)

# set up firebase tables
# NOTE each get request returns a dict
worksets = firebase.get('workset', None)
sheet_objects = firebase.get('sheet_objects', None)
sheet_objects_student = firebase.get('sheet_objects_student', None)

all_so = {}  # all non-orphaned sheet_object instances mapped to the workset they belong to
orphaned_so = []  # list of orphaned sheet_object instances
orphaned_sos = []  # list of orphaned sheet_object_student instances

# find orphans amongst sheet_object instances
for k in worksets.keys():
    for j in worksets[k].keys():
        all_so[j] = k
for k in sheet_objects.keys():
    if k not in all_so:
        orphaned_so.append(k)

# Find orphans amongst sheet_object_student instances
for k in sheet_objects_student.keys():
    if k not in sheet_objects:
        orphaned_sos.append(k)

# OUTPUT
if len(orphaned_so) == 0:
    print 'No orphaned sheet_object instances found!'
else:
    print 'Orphaned sheet_object instances:'
    for i in range(0, len(orphaned_so)):
        print orphaned_so[i]

if len(orphaned_sos) == 0:
    print 'No orphaned sheet_object_student instances found!'
else:
    print '\nOrphaned student_sheet_object instances:'
    for i in range(0, len(orphaned_sos)):
        print orphaned_sos[i]
